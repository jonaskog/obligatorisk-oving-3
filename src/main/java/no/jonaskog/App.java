package no.jonaskog;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

/**@author Jolse
 * JavaFX App
 */
public class App extends Application {


    @Override
    public void start(Stage stage) throws IOException {

        TilePane root = new TilePane();

        Button b1 = new Button("Nye kort");
        root.getChildren().addAll(b1);

        //Skjer når b1 blir aktivert
        b1.setOnAction(ActionEvent ->  {
            Spill spill = new Spill();
            spill.play();

            root.getChildren().clear();//sletter nodes
            Label k0 = new Label(spill.playerHandAt(0).getAsString());
            Label k1 = new Label(spill.playerHandAt(1).getAsString());
            Label k2 = new Label(spill.playerHandAt(2).getAsString());
            Label k3 = new Label(spill.playerHandAt(3).getAsString());
            Label k4 = new Label(spill.playerHandAt(4).getAsString());
            Label score = new Label("Score: "+ spill.player.getScoreStream()+ ".");
            Label flush = new Label("Har du flush?: "+spill.player.checkForFlush());
            Label hearts = new Label("Hjerte kort: "+ spill.player.hearts());
            Label QSpades= new Label("Dame ruter? : "+ spill.player.checkForS12());
            root.getChildren().addAll(k1,k2,k3,k4,k0,b1,score,flush,hearts,QSpades);

        });

        //pynt
        stage.setWidth(500);
        stage.setHeight(500);
        root.setAlignment(Pos.CENTER);
        root.setVgap(30.0);
        root.setVgap(30);

        Scene scene = new Scene(root,500,500);
        stage.setScene(scene);
        stage.setTitle("Card Game");
        stage.show();
    }



    public static void main(String[] args) {

        launch(args);



    }
}
