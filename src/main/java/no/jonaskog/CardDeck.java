package no.jonaskog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * @author Jolse
 * Kortstokk klasse
 */

public class CardDeck {
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private ArrayList<PlayingCard> cards;

    /**
     * Konstruktør for et carddeck
     * Konstruktøren lager en arraylist med kort fra 2-13 i med hver suit
     */
    public CardDeck() {
        cards = new ArrayList<PlayingCard>();
        for (char suit : suit) {//suit fra liste med suit
            for (int i = 2; i <= 13; i++) {
                PlayingCard etKort = new PlayingCard(suit, i);// kort i fra 2 - 13 og suit,
                cards.add(etKort);
            }
        }
    }

    // Akksessor
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }



    /**
     * Funksjon for trekke kort
     * @param n antall kort ønsket
     * @return ArryList med nye kort.
     */
    public ArrayList<PlayingCard> drawCards(int n) {
        ArrayList<PlayingCard> nyeKort = new ArrayList<>();
        ArrayList<Integer> valgteTall = new ArrayList<>();// lagrer randomtall.

        for (int i = 0; i<n; i++){

        Random rand = new Random();
        int random = rand.nextInt(48);//randomtall mellom 0-48
        if(valgteTall.contains(random)){//sjekker om det random tallet allerede er valgt.
            i--;// om tallet er valgt tidligere så går tilbake en itterasjon
            continue;
        }
        else{
            PlayingCard randomCard = cards.get(random);//randomtall som gir indexen i arrylisten.
            valgteTall.add(i,random);//legger til valgte randomtallet i posisjon "i" i arraylisten
            nyeKort.add(randomCard);

            }
        }
        return nyeKort;
    }



}