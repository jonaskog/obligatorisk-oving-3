package no.jonaskog;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**@author Jolse
 * Player class / altså kortene til spilleren
 * med funskjoner for å sjekke kortene
 */
public class Player {
    private ArrayList<PlayingCard> player ;

    public Player() {
        this.player = new ArrayList<PlayingCard>();
    }

    /**
     * Funksjon for å skrive å få ut en arrayListe med Player sine kort
     * @return Arrylist med kort for player
     */
    public ArrayList<PlayingCard> getPlayerCards() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.addAll(player);
        return cards;
    }

    /**
     * Legger til kort fra en Arryliste med Kort.
     * @param cards
     */
    public void addPlayerCards(ArrayList<PlayingCard> cards) {
        for (PlayingCard card:cards){
            player.add(card);
        }
    }

    public void setPlayerCard(PlayingCard card) {
        player.add(card);
    }

    /**
     * Finner hvor mange kort player har
     * @return  size
     */
    public int playerHandSize(){
        return player.size();
    }


    /**
     * Methode for checking if flush
     * @return true if flush, false if not
     */
    public boolean checkForFlush(){
        boolean flush= false;
        for (int i = 0 ; i < playerHandSize();i++){
            if(player.get(i).getSuit() == player.get(0).getSuit()){//sjekker alle kort mot første kortet i listen.
                flush = true;
            }
            else {
                flush = false;
                break;
            }
        }
        return flush;
    }

    /**
     * Adds alle face values
     * @return hand score
     */
    public int getScore(){
        int score = 0;
        for (PlayingCard card1 : player){
            score += card1.getFace();
        }
        return score;
    }
    /**
     * Finner summen av kortene
     * ved bruk av streams
     * @return sum faces
     */
    public String getScoreStream(){
        return String.valueOf(player.stream().map(PlayingCard::getFace).reduce(Integer::sum).get());
    }

    /**
     * Funksjon for å finne hjertekort på hånden
     */
    public String hearts() {
        StringBuilder hjerte = new StringBuilder();
        player.stream().filter(p ->p.getSuit() == 'H').forEach(p->hjerte.append(p.getAsString()).append(", "));
        return hjerte.toString();
    }


    /**
     * Funksjon sjekker etter Spar dame
      * @return true hva spardame, false hvis ikke
     */
    public Boolean checkForS12(){
        if(player.stream().anyMatch(playingCard -> playingCard.getAsString().equals("S12"))){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("-----Hand-----\n");
        for (PlayingCard c : player) {
            string.append(c.getAsString());
        }
        return string.toString();
    }
}
