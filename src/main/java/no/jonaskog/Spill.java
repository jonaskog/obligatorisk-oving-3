package no.jonaskog;

import java.util.ArrayList;

/**
 * @author Jolse
 * Klasse for å starte spillet
 * Slippper å bruke spill komandoer i application
 */
public class Spill {
    Player player = new Player();
    CardDeck cardDeck = new CardDeck();



    /**
     * Trekker 5 kort for spiller
     */
    public void play(){
        player.addPlayerCards(cardDeck.drawCards(5));
    }

    /**
     * Finner kort på gitt index
     * @param index
     * @return et kort
     */
    public PlayingCard playerHandAt(int index){
        String kort = " ";
        return player.getPlayerCards().get(index);
    }
}


