package no.jonaskog;

import org.junit.Test;

public class CardDeckTest {

    @Test
    public void drawCards() {
        var testDeck = new CardDeck();
        var testHand = new Player();
        testHand.addPlayerCards(testDeck.drawCards(52));

        assert (testHand.playerHandSize() == 52);

    }



}