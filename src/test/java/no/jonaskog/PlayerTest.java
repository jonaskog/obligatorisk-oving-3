package no.jonaskog;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    CardDeck cardDeckTest = new CardDeck();



    @Test @DisplayName("Add cards Test")
    void addPlayerCards() {
        var playerTest = new Player();
        playerTest.addPlayerCards(cardDeckTest.drawCards(5));
        assert (playerTest.playerHandSize() == 5);
    }

    @Test
    void checkForS12() {
        var PlayerTest = new Player();
        PlayingCard cardTest = new PlayingCard('S',12);
        PlayerTest.setPlayerCard(cardTest);
        assertEquals(cardTest,PlayerTest.getPlayerCards().get(0));
    }
}